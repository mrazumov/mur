.pragma library

/**
 * The function returns the name of the icon by index (for toolbar generation).
 *
 * Folder "icons" in project root contains icons.
 */
function getNameByIndex(index) {
    var iconsNames = ["folder", "file", "ok_circle", "delete", "arrow_forward",
                     "settings", "circle-help-question-mark-outline-stroke"];
    if (0 <= index && index < iconsNames.length) {
        return iconsNames[index] + ".svg";
    } else {
        throw "Out of range";
    }
}
