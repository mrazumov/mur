import QtQuick 2.6
import QtQuick.Controls 2.0

MainPageForm {
    onPressedToolbarBtn: {
        currPressedBtn.highlighted = true
    }

    onCanceledToolbarBtn: {
        if (currPressedBtn !== null) {
            currPressedBtn.highlighted = false
        }
    }

    projectMA.onMouseXChanged: {
        if (projectMA.drag.active) {
            projectPane.width += projectMA.mouseX
            if (projectPane.width < 50) {
                projectPane.width = 50
            }
        }
    }

    debugMA.onMouseYChanged: {
        if (debugMA.drag.active) {
            debugPane.height -= debugMA.mouseY
            if (debugPane.height >= 500) {
                debugPane.height = 500
            }
        }
    }
}
