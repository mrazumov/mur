import QtQuick 2.6
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
import "../js/iconsNames.js" as IconsName

Item {
    property alias projectMA : projectMA
    property alias debugMA: debugMA
    property alias debugPane: debugPane
    property alias projectPane: projectPane

    property Button currPressedBtn: null

    signal pressedToolbarBtn()
    signal canceledToolbarBtn()

    anchors.fill: parent

    RowLayout {
        anchors.fill: parent

        ToolBar {
            id: toolbar           
            anchors {top: parent.top; bottom: parent.bottom}

            background: Rectangle {
                color: Material.color(Material.Brown)
            }

            ColumnLayout {
                id: toolLayout

                Repeater {
                    id: repBtn

                    model: 7
                    ToolButton {
                        onPressed: {
                            canceledToolbarBtn()
                        }

                        onReleased: {
                            currPressedBtn = repBtn.itemAt(index)
                            pressedToolbarBtn()
                        }

                        Image {
                            anchors.fill: parent
                            source: "qrc:/icons/" + IconsName.getNameByIndex(index)
                        }
                    }
                }
            }
        }

        Pane {
            id: projectPane
            width: 0
            anchors {top: parent.top; bottom: parent.bottom; left: toolbar.right}

            background: Rectangle {
                color: Material.color(Material.Cyan)
                implicitWidth: parent.width
            }

            Rectangle {
                width: 18; height: 18
                radius: 18
                color: "black"
                anchors.horizontalCenter: parent.right
                anchors.verticalCenter: parent.verticalCenter

                MouseArea {
                    id: projectMA
                    anchors.fill: parent
                    drag {target: parent; axis: Drag.XAxis}
                }
            }
        }

        Pane {
            id: debugPane
            height: 0
            anchors {right: parent.right; bottom: parent.bottom; left: projectPane.right}

            background: Rectangle {
                color: Material.color(Material.Brown)
                implicitHeight: parent.height
            }

            Rectangle {
                width: 18; height: 18
                radius: 18
                color: "black"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.top

                MouseArea {
                    id: debugMA
                    anchors.fill: parent
                    drag {target: parent; axis: Drag.YAxis}
                }
            }
        }

        Pane {
            id: codePane
            anchors {top : parent.top; right: parent.right; bottom: debugPane.top; left: projectPane.right}

            background: Rectangle {
                color: Material.color(Material.Blue)
            }
        }
    }
}
