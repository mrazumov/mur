import QtQuick 2.6
import QtQuick.Window 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0

ApplicationWindow {
    visible: true
    width: 1024
    height: 820
    title: qsTr("BUG")

    id: m
    Material.theme: Material.Dark
    Material.accent: Material.Blue

    MainPage {
        id: v
    }

    onVisibleChanged: {
        v.projectPane.width = m.width  * 0.15
        v.debugPane.height  = m.height * 0.15
    }
}
