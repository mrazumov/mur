QT += qml quick

CONFIG += c++11

SOURCES += main.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

QT_QUICK_CONTROLS_STYLE=material ./app

DISTFILES += \
    icons/arrow_forward.svg \
    icons/circle-help-question-mark-outline-stroke.svg \
    icons/common-search-lookup-glyph.svg \
    icons/delete.svg \
    icons/file.svg \
    icons/folder.svg \
    icons/ok_circle.svg \
    icons/playstation-flat-icon-circle-black-and-white.svg \
    icons/playstation-flat-icon-cross-black-and-white.svg \
    icons/playstation-flat-icon-square-black-and-white.svg \
    icons/playstation-flat-icon-triangle-black-and-white.svg \
    icons/settings.svg \
    js/iconsNames.js
